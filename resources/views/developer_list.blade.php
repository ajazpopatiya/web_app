@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
@endsection
@section('content')
<div class="container">
  <button style="margin-bottom: 10px" class="btn btn-primary delete_all" data-url="{{ url('deleteAll') }}">Delete All Selected</button>
	<table class="table table-responsive"  id="myTable">
		<thead>
			<tr>
				<th></th>
				<th>No</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Phone Number</th>
				<th>Address</th>
				<th>Image</th>
				<th style="text-align: center;">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($developers as $key => $developer)
			<tr id="tr_{{$developer->id}}">
          <td><input type="checkbox" class="sub_chk" data-id="{{$developer->id}}"></td>
          <td>{{ ++$key }}</td>
					<td><?=$developer->first_name?></td>
					<td><?=$developer->last_name?></td>
					<td><?=$developer->email?></td>
					<td><?=$developer->phone_number?></td>
					<td><?=$developer->address?></td>
          <td><img src= {{ asset('img/'.$developer->image) }} alt="Card image cap" height="50px;" width="50px;"></td>
					<td style="text-align: center;"><a href="{{route('edit',$developer->id)}}"><span class="btn btn-primary">Edit  </span></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection
@section('script')
<script src="js/developer.js"></script>
@endsection
