@extends('layouts.app')
@section('title','Forgot Password')
@section('content')
<!-- start: REGISTER -->
<div class="row">
    <div class="main-login col-xs-12 col-sm-12">
        <div class="logo text-center">
            
            <h2>Logo</h2>
        </div>
        
        <!-- start: REGISTER BOX -->
        <div class="box-login">
            <h3>Forgot Password?</h3>
         
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
            <form method="POST" action="{{route('password.email') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    {!! Form::label('email','Enter your e-mail ID to reset your password')!!}
                    {{ Form::email('email',old('email'),['id'=>'email','class'=>'form-control','placeholder'=>'Enter Email Address']) }}
                </div>
                @if ($errors->has('email'))
                                    <span class="help-block ">
                                        <strong style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                <div class="form-actions form-group">
                    {!! Form::submit('Send password reset link',['class'=>'btn btn-block btn-light-blue'])!!}
                    </div>
                <div class="text-center">
                    Go for Login? 
                    <a href="{{route('login')}}">Log-in</a>
                </div>
            </form>
            <!-- start: COPYRIGHT -->
            <div class="copyright">
                2018 &copy; Leon-HRM
            </div>
            <!-- end: COPYRIGHT -->
        </div>
        <!-- end: REGISTER BOX -->
    </div>
</div>
<!-- end: REGISTER -->
@endsection
