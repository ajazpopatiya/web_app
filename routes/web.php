<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>'auth'], function(){
Route::get('/developer', 'DeveloperController@create')->name('developer');
Route::post('/developer_store', 'DeveloperController@store')->name('developer_store');
Route::get('/developer_list', 'DeveloperController@show')->name('developer_list');
Route::get('/edit/{edit}', 'DeveloperController@edit')->name('edit');
Route::post('/update/{update}', 'DeveloperController@update')->name('update');
Route::get('/delete/{delete}', 'DeveloperController@destroy')->name('delete');
Route::get('/deleteAll', 'DeveloperController@deleteAll')->name('deleteAll');
});
