$('.GlobalFormValidation').parsley();
$(document).ready(function() {
    $('.GlobalFormValidation').on("submit", function(e) {
        var form = $(this);
        e.preventDefault();
        var formData  = new FormData(this);
        $.ajaxSetup({
    			headers: {
    				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    			}
    		});
        $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(json) {
                    if (json.status == 'success') {
                        $('#Modal_Edits').modal('show');
                        $('#Modal_Edits').find('.modal-title').text(json.status);
                        $('#Modal_Edits').find('.modal-body').text(json.message).css('color','black');
                        setTimeout(function () {
              					window.location.href = json.url;
              				}, 3000);
                    }
                    if (json.status == 'duplicate_email') {
                        $('#Modal_Edits').modal('show');
                        $('#Modal_Edits').find('.modal-title').text('Duplicate');
                        $('#Modal_Edits').find('.modal-body').text(json.message).css('color','red');
                    }
                    // oTable.draw();
                },
                error: function(json) {
                    $('#Modal_Edits').modal();
                    $('#Modal_Edits').find('.modal-title').text('Error');
                    $('#Modal_Edits').find('.modal-body').html(json.responseText).css('color','red');
                    setTimeout(function() {
                        form.find('input[type=submit]').prop('disabled', false).removeClass('disabled');
                        form.find('#error').addClass('hidden');
                    }, 6000);

                },
                dataType: "json"
            });
        // }


    });

    $('.table').on('click','.delete_row', function(){
  		var id = $(this).attr('data-id');
  		$(this).closest('tr').remove();
  		$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
          $.ajax(
      	{
          url: "delete/"+id,
          type: 'get', // replaced from put
          dataType: "JSON",
          success: function (response)
          {
              console.log(response); // see the reponse sent
          },
          error: function(xhr) {
           console.log(xhr.responseText); // this line will save you tons of hours while debugging
          // do something here because of error
         }
      });

  });
});
