<?php

namespace App\Http\Controllers;

use App\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('developer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        $developers = $request->all();
        $file = $request->file('image');
        if ($request->hasFile('image')) {
            $fileName = time() . '.' . request()->image->getClientOriginalExtension();
            $file->move('img/', $fileName);
            $developers['image'] = $fileName;
        }
        $student = Developer::create($developers);

        return response()->json([
                    'status' => 'success',
                    'message' => 'Successfully Created',
                    'url' => '/developer_list',

        ]);
      } catch (\Exception $e) {
        return response()->json([
          'status' => 'duplicate_email',
          'message' => 'This email is already used',
        ]);
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function show(Developer $developer)
    {
      $developers = Developer::all();
      return view('developer_list', compact('developers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function edit(Developer $developer, $id)
    {
      $developer_edit = Developer::findOrFail($id);
      return view('developer', ['developer_edit' => $developer_edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Developer $developer, $id)
    {
      try {
        $developer_update = Developer::find($id);
        $all = $request->all();
        if (empty($all->image)) {
            $all['image'] = $developer_update->image;
        }
        $file = Input::file('image');
        if (Input::hasFile('image')) {
            $fileName = time() . '.' . request()->image->getClientOriginalExtension();
            $oldFilelogo = public_path('img') . $developer_update->image;
            if (is_file($oldFilelogo)) {
                unlink($oldFilelogo);
            }
            $file->move('img/', $fileName);
            $all['image'] = $fileName;
        } else {
            $fileName = $developer_update->image;
        }
        $developer_update->first_name = $all['first_name'];
        $developer_update->last_name = $all['last_name'];
        $developer_update->email = $all['email'];
        $developer_update->phone_number = $all['phone_number'];
        $developer_update->address = $all['address'];
        $developer_update->image = $all['image'];
        $developer_update->save();

        return response()->json([
                    'status' => 'success',
                    'message' => 'Successfully Updated',
                    'url' => '/developer_list',
        ]);
      } catch (\Exception $e) {
        return response()->json([
          'status' => 'fail',
          'message' => $e->getMessage(),
        ]);
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Developer  $developer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Developer $developer)
    {

    }

    public function deleteAll(Request $request, Developer $developer)
    {
        $ids = $request->ids;
        $developer->whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Deleted successfully."]);
    }
}
